import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.swing.*;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class SeleniumTestTest {
    @Test
    public void UseIfameElement() throws InterruptedException {

        System.setProperty( "webdriver.chrome.driver", "C:\\Users\\004558\\chromedriver.exe" );

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        webDriver.get( "http://web.stanford.edu/group/msande-news/cgi-bin/test/hype/iFrame_communication_HTML/index.html" );


        webDriver.switchTo().frame( 0 );

        WebDriverWait waitButton = new WebDriverWait( webDriver, 5 );
        waitButton.until( ExpectedConditions.visibilityOfElementLocated( By.xpath( "/html/body/div/div[2]/div[1]/div" ) ) ).click();

        webDriver.switchTo().defaultContent();

        Thread.sleep( 5000 );

        String expectedHeader = "It works!";
        assertEquals( expectedHeader, webDriver.findElement( By.xpath( "/html/body/div/div[2]/div[1]/div/div[2]" ) ).getText() );

        webDriver.close();

    }

    @Test
    public void OpenNewWindow() {
        System.setProperty( "webdriver.chrome.driver", "C:\\Users\\004558\\chromedriver.exe" );

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        webDriver.get( "https://turbo.az/" );

        WebDriverWait waitBoss = new WebDriverWait( webDriver, 5 );
        waitBoss.until( ExpectedConditions.visibilityOfElementLocated( By.xpath( "/html/body/div[2]/div[2]/div[2]/div/a[3]" ) ) ).click();


        Set<String> handles = webDriver.getWindowHandles();
        Iterator<String> it = handles.iterator();

        while (it.hasNext()) {
            String turbo = it.next();
            String boss = it.next();
            webDriver.switchTo().window( boss );

            String expectedHeader = "Boss.az";
            assertEquals( expectedHeader, webDriver.findElement( By.xpath( "/html/body/div[2]/div/a" ) ).getText() );

            webDriver.close();
            webDriver.switchTo().window( turbo );
        }

        webDriver.close();

    }

    @Test
    public void UploadDownloadFileDropdown() {

        System.setProperty( "webdriver.chrome.driver", "C:\\Users\\004558\\chromedriver.exe" );

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        webDriver.get( "https://www.freefileconvert.com/" );

        WebDriverWait webDriverWait = new WebDriverWait( webDriver, 10 );

        // UPLOAD THE FILE
        webDriver.findElement( By.xpath( "//*[@id=\"first_file\"]" ) ).
                sendKeys( "C:\\Users\\004558\\SeleniumHomeWork2\\src\\test\\resources\\FileForDownload.docx" );
        String expectedFile = webDriver.findElement( By.xpath( "//*[@id=\"first_file\"]" ) ).getAttribute( "value" );

        // CHOOSE FROM DROPDOWN LIST
        webDriver.findElement( By.xpath( "//*[@id=\"file-input-form\"]/div/div[2]/div/select" ) ).click();
        WebElement documentType = webDriver.findElement( By.cssSelector( "#file-input-form > div > div:nth-child(2) > div > select > option:nth-child(28)" ) );
        documentType.click();

        WebElement convertElement = webDriver.findElement( By.cssSelector( "#file-input-form > div > div:nth-child(3) > div > input" ) );
        convertElement.click();

        webDriver.manage().timeouts().implicitlyWait( 20, TimeUnit.SECONDS );

        // DOWNLOAD THE FILE

        WebDriverWait waitConvertedFile = new WebDriverWait( webDriver, 5 );
        waitConvertedFile.until( ExpectedConditions.visibilityOfElementLocated( By.xpath( "/html/body/div/div[3]/div/table/tbody/tr[2]/td[4]/a[1]" ) ) ).click();

        String downloadedFileName = "FileForDownload.docx";
        assertEquals( downloadedFileName, webDriver.findElement( By.xpath( "/html/body/div/div[3]/div/table/tbody/tr[2]/td[1]" ) ).getText() );


        webDriver.close();
    }


    @Test
    public void DoubleClick() {
        System.setProperty( "webdriver.chrome.driver", "C:\\Users\\004558\\chromedriver.exe" );

        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        webDriver.get( "https://demoqa.com/buttons" );
        webDriver.manage().timeouts().implicitlyWait( 20, TimeUnit.SECONDS );

        Actions actions = new Actions( webDriver );
        WebElement elementLocator = webDriver.findElement( By.id( "doubleClickBtn" ) );
        actions.doubleClick( elementLocator ).perform();


        String clickMessage = "You have done a double click";
        assertEquals( clickMessage, webDriver.findElement( By.xpath( "/html/body/div/div/div/div[2]/div[2]/div[1]/p" ) ).getText() );

        webDriver.close();

    }
}
